<?php

namespace Database\Seeders;

use App\Models\Organization;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organizations = Organization::all();
        $products = Product::all();

        foreach ($organizations as $organization) {
            foreach ($products as $product) {
                $organization->products()->attach($product->id, [
                    'price' => rand(100, 1000),
                ]);
            }
        }
    }
}
