<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductDetail extends Pivot
{
    use HasFactory;

    protected $table = 'product_details';
}
