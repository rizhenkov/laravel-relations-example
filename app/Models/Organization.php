<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_details')
            ->using(ProductDetail::class)
            ->as('detail')
            ->withPivot('price')
            ->withTimestamps();
    }
}
