<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $appends = ['price'];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'product_details')
            ->using(ProductDetail::class)
            ->as('detail')
            ->withPivot('price')
            ->withTimestamps();
    }

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function getPriceAttribute()
    {
        if($this->detail) {
            return $this->detail->price;
        }

        return null;
    }
}
