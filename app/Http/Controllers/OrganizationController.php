<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function index()
    {
        return response()->json(
            Organization::query()->with(['products'])->get()
        );
    }

    public function view($id)
    {
        $organization = Organization::find($id);

        return response()->json([
            'name' => $organization->name,
            'products' => $organization->products()->orderBy('price', 'desc')->get(),
        ]);
    }
}
