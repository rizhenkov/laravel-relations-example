<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\Product;
use Illuminate\Support\Arr;

class ProductController extends Controller
{
    public function index()
    {
        $organization = Organization::with(['products' => function ($q) {
                $q->orderBy('price', 'desc');
            }])
            ->findOrFail((int) request('organization_id'));

        $products = $organization->products->take(10);

        return response()->json([
            "results" => $products,
        ]);
    }

    // Same as index but more effective
    public function index2()
    {
        $organization = Organization::findOrFail((int) request('organization_id'));

        $products = $organization->products()
            ->orderBy('price', 'desc')
            ->limit(10)
            ->get();

        return response()->json([
            "results" => $products,
        ]);
    }

    public function add()
    {
        $product = new Product();
        $product->name = request('name');
        $product->save();

        foreach (request('details', []) as $detail) {
            // Attach as "Many to Many" relationship
            $product->organizations()->attach(Arr::get($detail, 'organization_id'), [
                'price' => Arr::get($detail, 'price'),
            ]);
        }

        return response()->json([
            "message" => "Product created successfully",
            "product" => $product->fresh(['organizations']),
        ]);
    }
}
