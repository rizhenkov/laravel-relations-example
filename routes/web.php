<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products', [
    'uses' => 'App\Http\Controllers\ProductController@index',
]);

Route::get('/products2', [
    'uses' => 'App\Http\Controllers\ProductController@index2',
]);

Route::post('/product', [
    'uses' => 'App\Http\Controllers\ProductController@add',
]);

Route::get('/organizations', [
    'uses' => 'App\Http\Controllers\OrganizationController@index',
]);

Route::get('/organization/{id}', [
    'uses' => 'App\Http\Controllers\OrganizationController@view',
]);


Route::get('/organization/{id}/products', [
    'uses' => 'App\Http\Controllers\OrganizationController@products',
]);
