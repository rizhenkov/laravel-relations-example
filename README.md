```terminal
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'

sail up

sail composer -V
 
sail artisan list
```
